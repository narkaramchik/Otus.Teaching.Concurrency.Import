using Newtonsoft.Json;
using NHibernate.Mapping.Attributes;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    public class Customer
    {

        [Id(0, Name = "Id")]
        [Generator(1, Class = "native")]
        [JsonProperty("id")]
        public int Id { get; set; }

        [Property(NotNull = true)]
        [JsonProperty("fullName")]
        public string FullName { get; set; }

        [Property(NotNull = true)]
        [JsonProperty("email")]
        public string Email { get; set; }

        [Property(NotNull = true)]
        [JsonProperty("phone")]
        public string Phone { get; set; }
    }
}