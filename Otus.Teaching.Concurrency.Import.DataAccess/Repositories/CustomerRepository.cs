using System;
using System.Collections.Generic;
using System.Reflection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.Attributes;
using NHibernate.Tool.hbm2ddl;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        public static string connectionString = "Host=localhost;Username=otus;Password=otus;Database=otus";
        public void AddCustomer(Customer customer)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(customer);
                    tx.Commit();
                }
            }
        }

        static ISessionFactory sessionFactory;
        static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                {
                    var configProperties = new Dictionary<string, string>
                    {
                        { NHibernate.Cfg.Environment.ConnectionDriver, typeof (NHibernate.Driver.NpgsqlDriver).FullName },
                        { NHibernate.Cfg.Environment.Dialect, typeof (NHibernate.Dialect.PostgreSQL82Dialect).FullName },
                        { NHibernate.Cfg.Environment.ConnectionString, connectionString },
                        //{ "show_sql", "true" }
                    };

                    var serializer = HbmSerializer.Default;
                    serializer.Validate = true;

                    var configuration = new Configuration()
                        .SetProperties(configProperties)
                        .AddInputStream(serializer.Serialize(Assembly.GetExecutingAssembly()));

                    new SchemaUpdate(configuration).Execute(true, true);

                    sessionFactory = configuration.BuildSessionFactory();
                }
                return sessionFactory;
            }
        }
    }
}