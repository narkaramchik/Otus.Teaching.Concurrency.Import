﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        public List<Customer> Parse(string path)
        {
            //Parse data
            var customerList = new List<Customer>();
            XmlSerializer formatter = new XmlSerializer(typeof(CustomersList));

            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                var customers = (CustomersList)formatter.Deserialize(fs);
                customerList = customers.Customers;
            }

            return customerList;            
        }
    }
}