﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConsoleApp
{
    class Program
    {
        private static string url = "https://localhost:44396";
        public static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml").Replace("ConsoleApp","WebApi");

        static void Main(string[] args)
        {
            Console.WriteLine("Создано 20 пользователей с идентификаторами от 1 до 20.\n" +
                "Для получения информации введите get {id}, " +
                "для добавления нового пользователя введите post {id}, " +
                "для выхода введите exit");
            while (true)
            {
                string s = Console.ReadLine();
                if (s.Contains("post"))
                {
                    if (int.TryParse(s.Replace("post ", ""), out int id))
                    {
                        AddRandomUser(id);
                    }
                    else Console.WriteLine("Введен неверный id."); 
                }
                else if (s == "exit")
                    break;
                else
                {
                    if (int.TryParse(s.Replace("get ", ""), out int id))
                    {
                        CallWebAPI(id);
                    }
                    else Console.WriteLine("Введен неверный id.");
                }
                Console.WriteLine("Введите следующую команду.");
            }

        }

        static void CallWebAPI(int id)
        {
            using (var client = new HttpClient())
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + "/api/Customer/" + id);
                request.Method = "GET";
                request.Accept = "application/json";
                request.AutomaticDecompression = DecompressionMethods.GZip;

                try
                {
                    HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse();
                    if (WebResponse.StatusCode == HttpStatusCode.OK)
                        using (Stream stream = WebResponse.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                var customer = (Customer)JsonConvert.DeserializeObject(reader.ReadToEnd(), typeof(Customer));
                                Console.WriteLine("Id: " + customer.Id + ", Name: " + customer.FullName + ", Email: " + customer.Email + ", Phone: " + customer.Phone);
                            }
                        }
                    else Console.WriteLine(WebResponse.StatusDescription);
                }
                catch(Exception e)
                {
                    Console.WriteLine("Не удалось получить пользователя.");
                    Console.WriteLine(e.Message);
                }
            }
        }

        static void AddRandomUser(int id)
        {
            var customer = RandomCustomerGenerator.Generate(1)[0];
            customer.Id = id;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + "/api/Customer");
            request.Method = "POST";
            request.AutomaticDecompression = DecompressionMethods.GZip;
            byte[] byteArray = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(customer));
            request.ContentLength = byteArray.Length;
            request.ContentType = "application/json";
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            try
            {
                HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse();
                if (WebResponse.StatusCode == HttpStatusCode.OK)
                {
                    addCustomer(customer);
                    Console.WriteLine("Добавлен пользователь - Id: " + customer.Id + ", Name: " + customer.FullName + ", Email: " + customer.Email + ", Phone: " + customer.Phone);
                }
                else Console.WriteLine(WebResponse.StatusDescription);
            }
            catch (Exception e)
            {
                Console.WriteLine("Не удалось добавить пользователя Id: " + customer.Id + ", Name: " + customer.FullName + ", Email: " + customer.Email + ", Phone: " + customer.Phone);
                Console.WriteLine(e.Message);
            }
        }

        static void addCustomer(Customer customer)
        {
            List<Customer> customerList = new List<Customer>();
            var xmlParser = new XmlParser();
            customerList = xmlParser.Parse(_dataFilePath);
            customerList.Add(customer);
            using var stream = File.Create(_dataFilePath);
            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
            {
                Customers = customerList
            });
        }
    }
}
