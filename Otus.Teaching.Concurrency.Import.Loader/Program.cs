﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        private static bool isExe = false;
        private static string generatePath = @"D:\Рабочий стол\C# Proffessional\параллелизм\DataGenerator\Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";

        static void Main(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                _dataFilePath = args[0];
                if (args.Length > 1)
                    if (int.TryParse(args[1],out int result))
                        if(result == 1)
                            isExe = true;

            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            if (isExe)
            {
                ProcessStartInfo procInfo = new ProcessStartInfo();
                procInfo.FileName = generatePath;
                procInfo.Arguments = _dataFilePath + ", 100";
                Process.Start(procInfo)?.WaitForExit();
            }
            else
            {
                GenerateCustomersDataFile();
            }
            List<Customer> customerList = new List<Customer>();
            var xmlParser = new XmlParser();
            customerList = xmlParser.Parse(_dataFilePath);

            var sw1 = new Stopwatch();
            sw1.Start();
            var loader1 = new ThreadPoolDataLoader(customerList, 10);
            loader1.LoadData();
            sw1.Stop();


            var sw2 = new Stopwatch();
            sw2.Start();
            var loader2 = new SimpleDataLoader(customerList);
            loader2.LoadData();
            sw2.Stop();

            Console.WriteLine($"Время работы с параллельной загрузкой - {sw1.Elapsed.TotalSeconds}, обычной - {sw2.Elapsed.TotalSeconds}");

        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 100);
            xmlGenerator.Generate();
        }
    }
}