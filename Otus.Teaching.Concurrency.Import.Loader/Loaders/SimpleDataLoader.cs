using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class SimpleDataLoader
        : IDataLoader
    {

        public List<Customer> customers = null;

        public SimpleDataLoader(List<Customer> _customers)
        {
            customers = _customers;
        }

        public void Init()
        {
            Console.WriteLine("������������� ��");
        }

        public void LoadData()
        {
            for (int i = 0; i < customers.Count; i++)
            {
                Thread.Sleep(100);
                Console.WriteLine("������� customer - " + customers[i].Id);
            }
        }
    }
}