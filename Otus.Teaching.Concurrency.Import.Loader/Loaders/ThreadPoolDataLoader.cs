using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class ThreadPoolDataLoader
        : IDataLoader
    {

        public List<Customer> customers;
        public int countPools;

        public ThreadPoolDataLoader(List<Customer> _customers, int _countPools = 5)
        {
            customers = _customers;
            countPools = _countPools;
        }

        public void LoadData()
        {
            int range = customers.Count / countPools;
            var handlers = new List<WaitHandle>();

            for(int i=0; i<countPools; i++)
            {
                var handler = new AutoResetEvent(false);
                handlers.Add(handler);

                ThreadPool.QueueUserWorkItem(AddCustomer, new State
                {
                    customers = customers.FindAll(item => item.Id > i*range && item.Id < (i+1)*range),
                    handle = handler
                });

            }
            WaitHandle.WaitAll(handlers.ToArray());

        }

        private void AddCustomer(object item)
        {
            if (!(item is State state))
            {
                return;
            }

            List<Customer> custs = state.customers;
            for (int i = 0; i < custs.Count; i++)
            {
                Thread.Sleep(100);
                Console.WriteLine("������� customer - " + custs[i].Id);
                //var CustomerRepo = new CustomerRepository();
                //CustomerRepo.AddCustomer(custs[i]);
            }
            state.handle.Set();
        }
    }

    public class State
    {
        public AutoResetEvent handle;
        public List<Customer> customers;
    }
}