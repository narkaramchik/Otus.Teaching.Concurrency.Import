﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ILogger<CustomerController> _logger;

        public CustomerController(ILogger<CustomerController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public IActionResult Post([FromBody] Customer customer)
        {
            List<Customer> customerList = new List<Customer>();
            var xmlParser = new XmlParser();
            customerList = xmlParser.Parse(WebApi.Program._dataFilePath);
            var a = customerList.Find(c => c.Id == customer.Id);
            if (a == null)
            {
                customerList.Add(customer);
                return Ok();
            }
            else
            {
                return Conflict();
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get(int id)
        {
            List<Customer> customerList = new List<Customer>();
            var xmlParser = new XmlParser();
            customerList = xmlParser.Parse(WebApi.Program._dataFilePath);
            var find = customerList.Find(cust => cust.Id == id);
            if (find == null)
                return NotFound();
            else return Ok(find);
        }

        [HttpGet()]
        public IEnumerable<Customer> Get()
        {
            List<Customer> customerList = new List<Customer>();
            var xmlParser = new XmlParser();
            customerList = xmlParser.Parse(WebApi.Program._dataFilePath);
            return customerList;
        }
    }
}
